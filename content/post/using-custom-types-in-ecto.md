---
title: "Using custom Types in Ecto"
date: 2017-04-01T22:26:46+01:00
tags: ["elixir"]
draft: true
---

This post focus on how to use Postgres composite types and how they can be implemented in Elixir using Ecto.

The problem to solve is find a good way to model a form which contains a date field that can either be a `timestamp` or a `string`.

Below you can see the form component used to gather the date data:

![Data Entry 1](/images/using-custom-type-in-ecto/data-entry-1.png)

![Data Entry 1](/images/using-custom-type-in-ecto/data-entry-2.png)

The date can be entered by selecting the date picker or by entering free text.

The following formats are accepted:

* When the date is provided:
  * `fulldate`: the format is `dd/mmm/yyyy`
  * `monthyear`: the format is `mmm/yyyy`
  * `yearonly`: the format is `yyyy`
* When the date is unspecified:
  * `ongoing`: The event is still happening.
  * `unknown`: The date of the event is unknown.

The user is not restricted to enter only a `timestamp` value, but the date picker allows also to enter `string` formats.

Given that the database doesn't accept complex types, the first and more naive solution to solve this problem is to save every entry as a `string`, because in our case this data doesn't need to be used to execute SQL query.

To save these dates in the database as `string` is an ok solution, but what can we do to model this data better?

## Postgres Composite Types

Postgres supports the use of `Composite Types` which allows to store structured fields, a composition of more than one simple type.

From Postgres documentation:

> 8.16. Composite Types
>
> A composite type represents the structure of a row or record; it is essentially just a list of field names and their data types. PostgreSQL allows composite types to be used in many of the same ways that simple types can be used. For example, a column of a table can be declared to be of a composite type.

Perfect, now we need to make this work nicely with our Elixir application, luckily Ecto allows to create custom types.

...but first

In order to model the data gathered in the form component, we want to model the

We must write the migration to create the composite type within Postgres.
The composite types are custom date we want to add two types:

* `custom_date_kind`: Type `ENUM` that allow the database to accept only the formats listed above
* `custom_date`: The composite type composed by
  * `date`: A `jsonb` type which have the `day`, `month` and `year` keys.
  * `kind`: the new `custom_date_kind`

```
defmodule MyApp.Repo.Migrations.AddCustomDateType do
  use Ecto.Migration

  def up do
    execute("""
    CREATE TYPE public.custom_date_kind AS ENUM ('ongoing', 'unknown', 'fulldate', 'monthyear', 'yearonly');
    """)

    execute("""
    CREATE TYPE public.custom_date AS (date jsonb, kind custom_date_kind);
    """)
  end

  def down do
    execute("""
    DROP TYPE public.custom_date;
    """)

    execute("""
    DROP TYPE public.custom_date_info;
    """)
  end
end
```

Now that the database is ready to store the new composite type, let's write the Ecto custom types.

## Ecto Custom Types

A new type in Ecto consists in a module that define the methods that allow the application to read and write the field defined as a composite type from and to the database.

The new type should have the `Ecto.Type` behaviour and implement the expected callbacks:

```
defmodule Ecto.CustomDate.Type do
  @behaviour Ecto.Type

  # Defines the name of the type created in the database
  @spec type() :: atom()

  # Cast the data from a map into a CustomDate.t() struct
  # to be used at runtime.
  @spec cast(map()) :: {:ok, CustomDate.t()} | :error

  # it receive the data from the database and it will map it
  # inside a CustomDate.t() to be store into the loaded struct.
  @spec load(tuple()) :: {:ok, CustomDate.t()} | :error

  # it receive the Date.t() from the struct schema at runtime and it
  # put it into a tuple to be send to the database.
  @spec dump(CustomDate.t()) :: {:ok, tuple()} | :error
end
```

Now that we know how the custom type should be implemented, we can start to write some code.

#### type/0

first we can implement the `type/0` function with the name of the database type

```
@spec type() :: atom()
def type, do: :custom_date
```

#### cast/1

We can now move to implement the `cast/1`. This function is called when we cast a struct into a `Changeset` and it convert the input into the format accepted by the database.

In our case we accept a map with the `date` and `kind` keys, and this need to be transformed in a `CustomDate.t()`

`CustomDate` is a structure used to incapsulate the data to be loaded and dumped in the database, the structure contains a `DateValue` and a `DateKind`, here below the modules with the data structure.

```
defmodule CustomDate do
  @type t :: %__MODULE__{date: DateValue.t(), kind: DateKind.t()}

  defstruct date: nil, kind: nil
end

defmodule DateValue do
  @type t :: %__MODULE__{day: integer(), month: integer(), year: integer()}

  defstruct day: nil, month: nil, year: nil
end

defmodule Custom.DateKind do
  @type t :: %__MODULE__{value: String.t()}

  defstruct value: nil
end
```

Now that we have all the needed structure in place we can write the `cast/1` function.
```
@spec cast(map()) :: {:ok, CustomDate.t()} | :error
def cast(%{"date" => nil, "kind" => kind}) do
  # Here the code to check if the kind is valid

  %CustomDate{date: nil, kind: %DateKind{value: kind}}
end

def cast(%{"date" => date, "kind" => kind}) do
  # Here the code to check if both the kind and the date are valid.
  # `date` can be in the following form:
  #   * %{"day" => day, "month" => month, "year" => year}
  #   * %{"month" => month, "year" => year}
  #   * %{"year" => year}
  %CustomDate{date: map_date(date), kind: %DateKind{value: kind}}
end

def cast(_), do: :error
```

In the cast function we can either pass no date value if the kind is either `ongoing` or `unknown`.
Other wise the date will me passed as a map and then using the `map_date/1` function it will be mapped into a DateValue.

#### load/1

The load function is used to process the raw data from the database and convert that into a custom type.

The date in the database is stored as `custom_date` composite type. This type contains a `jsonb` field and a `custom_date_kind` enum.

In our `load/1` we will then expect a tuple composed by a `map()`, which is how Ecto deserialise a jsonb field, and a `string()`.
Once we know the format expected, we can construct a `CustomDate` struct to be used in the Elixir application.

```
@spec load(tuple()) :: {:ok, CustomDate.t()} | :error
def load({nil, kind}) do
  {:ok, %CustomDate{date: nil, kind: %DateKind{value: kind}}}
end

def load({%{day: d, month: m, year: y}, kind}) do
  {:ok, %CustomDate{date: %DateValue{day: d, month: m, year: y}, kind: %DateKind{value: kind}}}
end

def load({_date, _kind}), do: :error
```

#### dump/1

the `dump/1` function receive an Ecto custom value, it validate the data and it serialise it into an Ecto native type before been saved into the database.

We receive as input a `CustomDate` which after been validated we transform in a tuple that can be saved as `custom_date` composite type.

```
@spec dump(CustomDate.t()) :: {:ok, tuple()} | :error
def dump(%CustomDate{date: nil, kind: %DateKind{value: value}}) do
  # Here the code to check if the CustomDate is valid
  {:ok, {nil, kind}}
end

def dump(%CustomDate{date: %DateValue{} = date, kind: %DateKind{value: value}}) do
  # Here the code to check if the CustomDate is valid
  {:ok, {date |> Map.from_struct(), kind}}
end

def dump(_) do
  :error
end
```

## Conclusions

Now that all the pieces are in place, we can finally use the new custom type to model the Ecto objects.

Inside the migration file, the database table can now contain fields defined with the new composite type.

```
create table(:reports) do
  add(:name, :string)
  add(:start_date, :custom_date)
  add(:end_date, :custom_date)
end
```

in the same way, when writing the Ecto struct, the schema callback should contain the previously defined `Ecto.CustomDate.Type` module when the custom date type are defined.

```ruby
schema "reports" do
    field(:name, :string)
    field(:start_date, Ecto.CustomDate.Type)
    field(:end_date, Ecto.CustomDate.Type)
end
```

Now we can save a new Report object using Ecto insert. When if we open the database console and query for the Reports, we can see the new raw been inserted as a valid custom_date composite type.

```sql
my_app_dev=# select * from reports limit 1;
 id | name | start_date                          |    end_date       
----+------+-------------------------------------+------------------
  1 | test | ("{""day"": 8, ""year"": 2018, ""month"": 5}",fulldate) | (null, ongoing)
```

Hopefully this post can help to shed some lights upon how `Ecto.Type` works and how the Ecto custom types can be used to model more flexible data and stored inside the Postgres composite types.
